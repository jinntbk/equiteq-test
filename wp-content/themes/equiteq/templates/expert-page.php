<?php
/* Template Name: Expert Page */
get_header();
$id = get_the_ID();
$page = get_post($id);
?>

<?php

/**Hero */
if(isset($page->banner_image)) {
hm_get_template_part('template-parts/hero', ['page' => $page]);
}

?>

<section class="bg-dark-blue">
    <div class="container text-white no-pad-gutters">
        <h3 class="text-uppercase mb-4"><?php echo $page->intro_title ?></h3>
        <div class="row">
            <div class="col-md-8 mb-4">
                <?php echo $page->post_content ?>
            </div>
        </div>
        <!--May implement the search and filter here-->
        <div class="team-top-section">
            <div class="page-center">
                <div class="deal-header">
                <h3><?=__('MEET OUR EXPERTS', 'equiteq');?></h3>
                </div>
                
                <div class="expert-cont">
                    <p><?=__('Deep knowledge, surprising insights, and a collaborative approach to growing and selling your knowledge-based business. Meet the team that has made Equiteq the leading global investment bank for the knowledge economy.', 'equiteq');?></p>
                </div>
                
                <div class="team-f-bottom">
                    <div class="team-left">
                        <div class="team-f-title">
                            <h5>
                                <?=__('FILTERS', 'equiteq');?>
                            </h5>
                        </div>
                        <div class="team-filter-box">
                            <div class="sector-filter filterbox">
                                <label id="typeListLabelSector"><span><?=__('Sector', 'equiteq');?></span> <i class="fa fa-caret-down"></i></label>
                                <ul data-id="type" class="typeListing" data-filter-group="sector" style="display: none;">
                                    <li class="selected sector-filter-item" data-filter="" data-value="" data-search="All">
                                        <a href="javascript:void(0)">All</a>
                                    </li>
                                    <?php
                                    $industries = get_industries();
                                    foreach ($industries as $industry) {
                                        $industryID = $industry->ID;
                                        $industryValue = $industry->post_name;
                                        $industryName = get_the_title($industryID);
                                        $industryIcon = get_field('icon', $industryID);
                                        ?>
                                        <li class="sector-filter-item" data-filter="<?=esc_html($industryName);?>">
                                            <a href="javascript:void(0)" data-value="<?=esc_html($industryID);?>"><?=esc_html($industryName);?></a>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                            <div class="location-filter filterbox">
                                <label id="typeListLabelLocation"><span><?=__('Location', 'equiteq');?></span> <i class="fa fa-caret-down"></i></label>
                                <ul data-id="type" class="typeListing" data-filter-group="location" style="display: none;">
                                    <li class="selected location-filter-item" data-filter="" data-value="" data-search="All">
                                        <a href="javascript:void(0)">All</a>
                                    </li>
                                    <?php
                                    $locations = get_locations();
                                    foreach ($locations as $location) {
                                        $locationID = $location->ID;
                                        $locationValue = $location->post_name;
                                        $locationName = get_the_title($locationID);
                                        ?>
                                        <li class="location-filter-item" data-filter="<?=esc_html($locationName);?>">
                                            <a href="javascript:void(0)" data-value="<?=esc_html($locationID);?>"><?=esc_html($locationName);?></a>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="team-right">
                        <label for="search" class="searchtext"><?=__('Search', 'equiteq');?></label>
                        <div class="input-group alt">
                            <input type="text" class="" id="quicksearch">
                            <span class="input-group-append">
                                <div class=""><i class="fa fa-search"></i></div>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--May implement the experts profile list here-->
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 expert-list">
            <?php
            $query = new WP_Query(array(
                'post_type' => 'expert',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'orderby' => 'menu_order',
                'order' => 'DESC',
            ));

            if($query->have_posts()):
                while($query->have_posts()) : $query->the_post();
                    get_template_part('template-parts/_components/expert-list-item');
                endwhile;
                wp_reset_postdata();
            else:
                echo '<p>No records found</p>';
            endif;
            ?>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();
?>